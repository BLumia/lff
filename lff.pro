QT       += core widgets
QT       -= gui

TARGET = lff

SOURCES += main.cpp

BINDIR = $$PREFIX/bin

target.path = $$BINDIR

autostart.path = /etc/xdg/autostart
autostart.files = lff-autostart.desktop

INSTALLS += target autostart
